# Financial Services – Financial API

The FAPI WG initially made efforts to develop international functional APIs for
open banking, however that effort fizzled out and the content of this
file has now been removed to avoid any potential for confusion.

For historical reference, the FAPI WG's final version of this specification can be viewed here:

https://bitbucket.org/openid/fapi/src/d7e780be14ee7c5e3f184d915d0d511a0ea6b1f5/Financial_API_WD_000.md

To view the FAPI working group's current documents see:

https://openid.net/wg/fapi/
