# FAPI 2.0 Security Profile

The FAPI 2.0 Security Profile was previously known as the FAPI 2.0 Baseline Profile. 

The latest HTML version of the document can be found here: https://openid.bitbucket.io/fapi/fapi-2_0-security-profile.html

The source on bitbucket can be found here: https://bitbucket.org/openid/fapi/src/master/FAPI_2_0_Security_Profile.md

