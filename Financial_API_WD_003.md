# Financial Services – Financial API - Part 3: Open Data API

The Financial-grade API working group's specifications defining functional APIs are deprecated and no longer maintained.

For historical reference, the FAPI WG's final version of this specification can be viewed here:

https://bitbucket.org/openid/fapi/src/d7e780be14ee7c5e3f184d915d0d511a0ea6b1f5/Financial_API_WD_003.md

To view the FAPI working group's current documents see:

https://openid.net/wg/fapi/
