#!/usr/bin/env python3
#
# Calls the docker build for each spec
# builds an index.html that links to them all
# reports any that fail
# exits with appropriate success / failure return code

import glob
import os
import re
import sys
import subprocess

dirs_exclude = [
    ".git",
    ".idea",
    "cds-spec-analysis"
]

# This list is currently a list of files that fail to process
# Some of them we likely don't care about, but some of them should be fixed and removed from the list
files_exclude = [
    './FAPI_2_0_Advanced_Profile.md',
    './FAPI_2_0_Advanced_Authorization_Profile.md',
    './FAPI_2_0_Baseline_Profile.md',
    './Financial_API_JWT_Secured_Authorization_Response_Mode.md',
    './Financial_API_Lodging_Intent.md',
    './Financial_API_Pushed_Request_Object.md',
    './Financial_API_Simple_HTTP_Message_Integrity_Protocol.md',
    './Financial_API_WD_000.md',
    './Financial_API_WD_001.md',
    './Financial_API_WD_002.md',
    './Financial_API_WD_003.md',
    './Financial_API_WD_004.md',
    './Financial_API_WD_005.md',
    './Financial_API_WD_CIBA.md',
    './TR-Cross_browser_payment_initiation_attack.md',
    './fapi-grant-management.md',
    './FAPI_1.0/changes-between-id2-and-final.md',
    './FAPI_2_0_Message_Signing.md',
    './FAPI_2_0_Security_Profile.md'
]

fapi1_files = [
    './FAPI_1.0/openid-financial-api-part-1-1_0.md',
    './FAPI_1.0/openid-financial-api-part-2-1_0.md'
]

failed = []

files_generated = []

def get_output_filename(fname):
    # get the output filename, i.e. do what https://github.com/oauthstuff/markdown2rfc/blob/master/make.sh#L18 does
    # and find the line like: value = "fapi-2_0-baseline-01"
    regex = r'^[\W]*value[\W]*=[\W]*"(.*)"'
    with open(fname, 'r') as f:
        for line in f:
            matches = re.search(regex, line)
            if matches:
                return matches.group(1)


### FAPI1 Markdown manipulation functions

# fast forward file pointer to text pattern
def ff_to_text(in_file, out_file, pattern):
    line = in_file.readline()
    while line:
        # process the line
        out_file.write(line)
        matches = re.search(pattern, line)
        if matches:
            break
        line = in_file.readline()

# fast forward file pointer to {mainmatter} text
def ff_to_mainmatter(in_file, out_file):
    ff_to_text(in_file, out_file, r'^\{mainmatter\}')

# removes appendix section numbers and move header level up one level e.g. '### A.1 Example' => '## Example'
def remove_appendix_section_numbers(in_file, out_file):
    appendix_pattern = re.compile(r'^(#+)(\s+)(.*\s*)([A-Za-z]\.(\d+\.*)*\s*)(.+\s*)$')
    line = in_file.readline()
    while line:
        # Appendix A. Examples
        ### A.1 Example request object
        matches = appendix_pattern.search(line)
        if matches:
            groups = list(matches.groups())
            # ('###', ' ', '', 'A.1 ', '1', 'Examples')
            del(groups[2:5])
            #groups.pop(4) # removes matched section numbers
            #groups.pop(3) # remove matched section numbers and following spaces ((\d+\.*)+\s*)
            #groups.pop(2) # Appendix (words before section number)
            groups[0] = groups[0][0: len(groups[0]) - 1] # shorten section '#' by 1
            out_file.write(''.join(groups))
        else:
            out_file.write(line)
        line = in_file.readline()


# removes section numbers and move header level up one level e.g. '### 1.0.1 Scope' => '## Scope'
def remove_section_numbers2(in_file, out_file):
    section_pattern = re.compile(r'^(#+)(\s+)((\d+\.*)+\s*)(.+\s*)$')
    backmatter_pattern = re.compile(r'\{backmatter\}')
    line = in_file.readline()

    while line:
        # match ## 1.0.1 Scope
        matches = section_pattern.search(line)
        if matches:
            groups = list(matches.groups())
            groups.pop(3)
            groups.pop(2) # remove first whitespace match
            groups[0] = groups[0][0 : len(groups[0]) - 1]
            out_file.write(''.join(groups))
        elif backmatter_pattern.search(line):
            out_file.write(line)
            remove_appendix_section_numbers(in_file, out_file)
        else:
            out_file.write(line)
        line = in_file.readline()

# Manipulates FAPI 1 markdown so it can be converted by MMark
def change_markdown(in_file, out_file):
    with open(in_file, 'r') as filein:
        with open(out_file, 'w') as fileout:
            ff_to_mainmatter(filein, fileout)
            remove_section_numbers2(filein, fileout)


def process_fapi1_spec(fname):
    fixedname = fname+'2'
    change_markdown(fname, fixedname)
    process_spec(fixedname)  # process the md2 file

### end FAPI1 manipulation funtions

def process_spec(fname):
    currentdir = os.getcwd()
    cmd = [ 'docker', 'run', '-v', currentdir+':/data', 'danielfett/markdown2rfc', fname ]
    print("Running: " + ' '.join(cmd))
    retcode = subprocess.call(cmd)
    if retcode != 0:
        failed.append(fname)
        print("docker run returned failure for "+fname)
        return
    outputfname = get_output_filename(fname)
    outputfname += ".html"
    if not os.path.isfile(outputfname):
        print("expected output file of "+outputfname+" not found for "+fname)
        failed.append(fname)
        return

    # the generated html contains a version number that we don't want to end up in the url; remove it
    m = re.search(r'^(.*)-\d\d\.html$', outputfname)
    if m:
        newoutputfname = m.group(1)+".html"
        os.rename(outputfname, newoutputfname)
        outputfname = newoutputfname
        print("Renamed output to "+outputfname)
    files_generated.append(newoutputfname)
    print()

def walk_tree():
    for root, dirs, files in os.walk("."):
        dirs[:] = [d for d in dirs if d not in dirs_exclude]
        for file in sorted(files):
            if not file.endswith(".md"):
                continue
            if file.casefold() == "readme.md".casefold():
                continue
            fullfname = os.path.join(root, file)
            if fullfname in files_exclude:
                continue
            if fullfname in fapi1_files:
                process_fapi1_spec(fullfname)
            else:
                process_spec(fullfname)

def generate_index():
    print("Creating index.html")
    header = '''
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>OpenID Foundation FAPI Working Group Drafts</title>
  <link rel="stylesheet" href="../base.css" type="text/css"/>
  <style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
.logo {
	max-width: 10%;
	max-height: auto;
	float: left;
	margin-right: 2em;
}
.not-active {
   pointer-events: none;
   cursor: default;
   color: #aF0000;
}
-->
  </style>
</head>
<body>
<div id="nav" class="column span-18 append-1 prepend-1">
  <ul class="navigation">
    <li><a href='https://openid.net/wg/fapi/'>About</a></li>
    <li><a href='https://bitbucket.org/openid/fapi/'>Repository</a></li>
    <li><a href="https://bitbucket.org/openid/fapi/issues?status=new&status=open">Issues</a></li>
  </ul>
</div>
<div id="content">
<h1>OpenID Foundation FAPI Working Group Drafts</h1>
<h2>List of Draft Specifications</h2>
<p>Below are links to the HTML versions of the working groups draft documents:</p>
<ul>
'''
    footer = '''
</ul>
</div>
</body>
</html>
'''
    with open('index.html', 'w') as f:
        print(header, file = f)
        for fname in files_generated:
            print('	 <li><a href="{}">{}</a></li>'.format(fname, os.path.splitext(fname)[0]), file = f)
        print(footer, file = f)
    return

walk_tree()
generate_index()
if failed:
    print("The processing of some specifications failed:")
    for f in failed:
        print(f)
    sys.exit(1)

sys.exit(0)
